import React from "react";
import BarNav from "../dashboard/BarNav";
import Background from "../dashboard/Background";
import TabBar from "../dashboard/Tab";
import { useState } from "react";

export default function Dashboard() {
	// let [count, setCount] = useState([
	// 	"a",
	// 	"b",
	// 	"c",
	// 	"d",
	// 	"e",
	// 	"f",
	// 	"g",
	// 	"h",
	// 	"i",
	// 	"j",
	// 	"k",
	// 	"l",
	// 	"m",
	// 	"n",
	// 	"o",
	// 	"p",
	// ]);

	let handleClick = () => {
		// let labels = count.splice(Math.floor(Math.random() * count.length), 9);
		console.log("Clicked");
	};

	let x = [
		"Amount Invested",
		"AAPL",
		"MFST",
		"GOOGL",
		"AMZN",
		"FB",
		"WMT",
		"BABA",
		"TSLA",
		"PYPL",
		// "JPM",
		// "NVDA",
		// "JNJ",
	];
	let labels = x.splice(Math.floor(Math.random() * x.length), 9);
	console.log(labels);

	return (
		<div>
			<div className="mx-auto md:w-11/12">
				<BarNav />
			</div>
			<div className="bg-white-gray">
				<div className="pb-4 mx-auto md:pb-16 md:w-11/12">
					<Background />
					<TabBar handleClick={handleClick} labels={labels} />
				</div>
			</div>
		</div>
	);
}
