import { LineChart, Line, CartesianGrid, XAxis, YAxis } from "recharts";

import { useState, useEffect } from "react";
const RenderLineChart = () => {
  const data = [
    { name: "Page A", uv: 400, pv: 2400, amt: 200 },
    { name: "Page B", uv: 200, pv: 1400, amt: 200 },
    { name: "Page B", uv: 200, pv: 500, amt: 200 },
    { name: "Page B", uv: 180, pv: 700, amt: 200 },
    { name: "Page B", uv: 140, pv: 870, amt: 200 },
  ];
  // Kishore bug fixed
  const [isSSR, setIsSSR] = useState(true);
  useEffect(() => {
    if (typeof window !== "undefined") {
      setIsSSR(false);
      // Client-side-only code
    }
  }, []);
  console.log(isSSR);
  return (
    // <LineChart width={800} height={300} data={data}>
    //   <Line type="linear" dataKey="uv" stroke="#8884d8" />
    //   <Line type="linear" dataKey="pv" stroke="#0095bf" />
    //   <CartesianGrid stroke="#ccc" />
    //   <XAxis dataKey="name" />
    //   <YAxis />
    // </LineChart>
    <div>
      {isSSR ? null : (
        <LineChart width={600} height={300} data={data}>
          <Line type="monotone" dataKey="uv" stroke="#8884d8" />
          <Line type="linear" dataKey="pv" stroke="#0095bf" />
          <CartesianGrid stroke="#ccc" />
          <XAxis dataKey="name" />
          <YAxis />
        </LineChart>
      )}
    </div>
  );
};

export default RenderLineChart;
