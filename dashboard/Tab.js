import { useState } from "react";
import { Tab } from "@headlessui/react";
import SolidBar2 from "./SolidBar2";
import SolidBar3 from "./SolidBar3";
import ColorCard from "./ColorCard";

function classNames(...classes) {
	return classes.filter(Boolean).join(" ");
}

export default function TabBar({ handleClick, labels }) {
	let [categories] = useState({
		Recent: [
			{
				id: 1,
				title: "Does drinking coffee make you smarter?",
				date: "5h ago",
				commentCount: 5,
				shareCount: 2,
				calc: "Calculator",
				end: <SolidBar2 labels={labels} />,
			},
		],
		Popular: [
			{
				id: 1,
				title: "Is tech making coffee better or worse?",
				date: "Jan 7",
				commentCount: 29,
				shareCount: 16,
				end: <SolidBar3 />,
			},
		],
		Trending: [
			{
				id: 1,
				title: "Ask Me Anything: 10 answers to your questions about coffee",
				date: "2d ago",
				commentCount: 9,
				shareCount: 5,
				end: <SolidBar2 labels={labels} />,
			},
		],
		Check: [
			{
				id: 1,
				title: "Ask Me Anything: 10 answers to your questions about coffee",
				date: "2d ago",
				commentCount: 29,
				shareCount: 15,
				end: <SolidBar3 />,
			},
		],
		Wall: [
			{
				id: 1,
				title: "Ask Me Anything: 10 answers to your questions about coffee",
				date: "2d ago",
				commentCount: 39,
				shareCount: 35,
				end: <SolidBar2 labels={labels} />,
				snd: <ColorCard />,
			},
		],
		Texture: [
			{
				id: 1,
				title: "Ask Me Anything: 10 answers to your questions about coffee",
				date: "2d ago",
				commentCount: 89,
				shareCount: 25,
				end: <SolidBar3 />,
			},
		],
	});

	return (
		<div className="px-2 pb-16 mx-auto md:w-11/12 sm:px-0">
			<Tab.Group>
				<Tab.List className="flex-wrap p-1 space-x-1 bg-white border md:flex md:flex-nowrap">
					{Object.keys(categories).map((category) => (
						<Tab
							key={category}
							className={({ selected }) =>
								classNames(
									"w-full py-2.5 text-sm font-medium leading-5 text-black shadow",
									"focus:outline-none",
									selected
										? "text-white p-4 bg-btn-color"
										: "bg-white text-black p-4"
								)
							}
						>
							{category}
						</Tab>
					))}
				</Tab.List>
				<Tab.Panels className="mt-2">
					{Object.values(categories).map((posts, idx) => (
						<Tab.Panel
							key={idx}
							className={classNames(
								" bg-white",
								"ring-white ring-opacity-60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2"
							)}
						>
							<div className="flex flex-col items-center justify-between p-4 border border-r md:flex-row">
								<p>Savings</p>
								<p>401K</p>
								<p>Insurance</p>
								<p>Tax</p>
								<p>Mortgage Refinance</p>
								<p
									style={{
										backgroundColor: "#e5e7eb",
										padding: "0 10px",
										borderRadius: "6px",
										padding: "5px 10px",
										color: "#04004d",
									}}
								>
									Investment
								</p>
							</div>

							<div style={{ margin: "0 15px" }}>
								<div className="my-4">
									<p className="text-2xl font-bold text-navy-dark">
										Investment Calculator
									</p>
								</div>
								<div className="w-full">
									<div className="flex flex-wrap items-center gap-8 my-4">
										<div>
											<p className="text-base font-bold text-btn-color">
												Monthly Investment
											</p>
											<input
												type="search"
												className="px-2 py-2 border"
												style={{ outline: "none" }}
												placeholder="$1000"
											></input>
										</div>
										<div>
											<p className="text-base font-bold text-btn-color">
												Yearly Investment
											</p>
											<input
												type="search"
												style={{ outline: "none" }}
												className="px-2 py-2 border"
												placeholder="2001"
											></input>
										</div>
									</div>
									<div>
										<button
											style={{ width: "39%" }}
											className="p-4 text-base font-semibold text-white rounded bg-btn-color"
											onClick={handleClick}
										>
											Calculate
										</button>
									</div>
								</div>
							</div>
							<ul>
								{posts.map((post) => (
									<li
										key={post.id}
										style={{ margin: "0 15px" }}
										className="relative p-1 rounded-md md:p-3"
									>
										<h3 className="text-sm font-medium leading-5">
											{/* {post.title} */}
											{/* {post.calc} */}
										</h3>
										<ul className="flex mt-1 space-x-1 text-xs font-normal leading-4 text-gray-500">
											{/* <li>{post.date}</li>
                      <li>&middot;</li>
                      <li>{post.commentCount} comments</li>
                      <li>&middot;</li>
                      <li>{post.shareCount} shares</li> */}
										</ul>
										{/* <a
                      href="#"
                      className={classNames(
                        "absolute inset-0 rounded-md",
                        "ring-blue-400 focus:z-10 focus:outline-none focus:ring-2"
                      )}
                    /> */}
										<div>{post.end}</div>
										<div>{post.snd}</div>
									</li>
								))}
							</ul>
						</Tab.Panel>
					))}
				</Tab.Panels>
			</Tab.Group>
		</div>
	);
}
