import React from "react";
import {
	Chart as ChartJS,
	CategoryScale,
	LinearScale,
	BarElement,
	Title,
	Tooltip,
	Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";
import faker from "faker";

export default function SolidBar2({ labels }) {
	ChartJS.register(
		CategoryScale,
		LinearScale,
		BarElement,
		Title,
		Tooltip,
		Legend
	);

	const options = {
		responsive: true,
		plugins: {
			legend: {
				position: "top",
			},
			title: {
				display: true,
				// text: "Chart.js Bar Chart",
			},
		},
	};

	// const labels = [
	// 	"Amount Invested",
	// 	"AAPL",
	// 	"MFST",
	// 	"GOOGL",
	// 	"AMZN",
	// 	"FB",
	// 	"WMT",
	// 	"BABA",
	// 	"TSLA",
	// 	"PYPL",
	// 	"JPM",
	// 	"NVDA",
	// 	"JNJ",
	// ];

	const data = {
		labels,
		datasets: [
			{
				label: "Dataset 1",
				data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
				backgroundColor: "#686694",
			},
		],
	};

	return <Bar options={options} data={data} />;
}
