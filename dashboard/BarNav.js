import React from "react";
import Image from "next/image";
import Link from "next/link";
import profile from "../styles/assets/profile.jpg";

export default function BarNav() {
  return (
    <div>
      <header>
        <div className="flex flex-wrap items-center justify-between h-24 p-4 px-4 mx-auto md:justify-center md:flex-nowrap lg:w-11/12">
          <div className="flex items-center">
            <Link href="/">
              <a>
                <Image src={"/logo.png"} alt="Logo" height={50} width={140} />
              </a>
            </Link>
            <div className="relative hidden md:block">
              <div
                style={{ paddingLeft: "10px" }}
                className="absolute inset-y-0 left-0 flex items-center pointer-events-none md:-top-8"
              >
                <Image
                  alt="stuff"
                  src={"/icon-search.svg"}
                  height={20}
                  width={20}
                ></Image>
              </div>
              <input
                type="search"
                id="default-search"
                style={{ paddingLeft: "40px" }}
                className="block w-full p-4 text-sm text-gray-900 border border-gray-300 rounded-b-none md:rounded-lg outline-0"
                placeholder="Ex- 401K, Financial Professional, Tax Advice"
                required
              />
            </div>
          </div>
          <div>
            <Image
              style={{ borderRadius: "50%" }}
              width={40}
              height={40}
              alt="user_profile"
              src={profile}
            />
          </div>
        </div>
        <div className="relative px-2 my-4 md:hidden md:px-0">
          <div
            style={{ paddingLeft: "20px" }}
            className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none md:-top-8"
          >
            <Image
              alt="stuff"
              src={"/icon-search.svg"}
              height={20}
              width={20}
            ></Image>
          </div>
          <input
            type="search"
            id="default-search"
            style={{ paddingLeft: "40px" }}
            className="block w-full p-4 text-sm text-gray-900 border border-gray-300 rounded-b-none md:rounded-lg outline-0"
            placeholder="Ex- 401K, Financial Professional, Tax Advice"
            required
          />
        </div>
      </header>
    </div>
  );
}
