import React from "react";

export default function Background() {
  return (
    <div className="py-8 mx-auto md:w-11/12">
      <p className="px-2 text-4xl font-bold text-navy-dark">My Dashboard</p>
    </div>
  );
}
