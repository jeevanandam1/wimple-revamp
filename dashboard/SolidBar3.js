import React from "react";
import {
	Chart as ChartJS,
	CategoryScale,
	LinearScale,
	PointElement,
	LineElement,
	Title,
	Tooltip,
	Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";
import faker from "faker";

export default function line() {
	ChartJS.register(
		CategoryScale,
		LinearScale,
		PointElement,
		LineElement,
		Title,
		Tooltip,
		Legend
	);

	const options = {
		responsive: true,
		plugins: {
			legend: {
				position: "top",
			},
			title: {
				display: true,
				// text: "Chart.js Line Chart",
			},
		},
	};

	const labels = [
		"2001",
		"2002",
		"2003",
		"2004",
		"2005",
		"2006",
		"2007",
		"2008",
		"2009",
		"2010",
		"2021",
	];

	const data = {
		labels,
		datasets: [
			{
				label: "Dataset 1",
				data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
				borderColor: "rgb(255, 99, 132)",
				backgroundColor: "rgba(255, 99, 132, 0.5)",
			},
			{
				label: "Dataset 2",
				data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
				borderColor: "rgb(53, 162, 235)",
				backgroundColor: "rgba(53, 162, 235, 0.5)",
			},
			{
				label: "Dataset 3",
				data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
				borderColor: "#e8c727",
				backgroundColor: "#e8c727",
			},
			{
				label: "Dataset 4",
				data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
				borderColor: "#00c076",
				backgroundColor: "#00c076",
			},
			{
				label: "Dataset 5",
				data: labels.map(() => faker.datatype.number({ min: 0, max: 1000 })),
				borderColor: "#5f0f40",
				backgroundColor: "#5f0f40",
			},
		],
	};

	return <Line options={options} data={data} />;
}
