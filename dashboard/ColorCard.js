import React from "react";

export default function ColorCard() {
  const arr = [
    {
      title: "AAPL",
      ttl: "$1000",
      crt: "$5000",
      dot: "#0184eb",
      id: 1,
    },
    {
      title: "MFST",
      ttl: "$1000",
      crt: "$5000",
      dot: "#00c076",
      id: 2,
    },
    {
      title: "JNJ",
      ttl: "$1000",
      crt: "$5000",
      dot: "#e8c727",
      id: 3,
    },
    {
      title: "AMZN",
      ttl: "$1000",
      crt: "$5000",
      dot: "#f68626",
      id: 4,
    },
    {
      title: "GOOGL",
      ttl: "$1000",
      crt: "$5000",
      dot: "#8856c3",
      id: 5,
    },
    {
      title: "FB",
      ttl: "$1000",
      crt: "$5000",
      dot: "#0f4c5c",
      id: 6,
    },
    {
      title: "WMT",
      ttl: "$1000",
      crt: "$5000",
      dot: "#5f0f40",
      id: 7,
    },
    {
      title: "BABA",
      ttl: "$1000",
      crt: "$5000",
      dot: "#9a031e",
      id: 8,
    },
    {
      title: "TSLA",
      ttl: "$1000",
      crt: "$5000",
      dot: "#f20089",
      id: 9,
    },
    {
      title: "PYPL",
      ttl: "$1000",
      crt: "$5000",
      dot: "#00f5d4",
      id: 10,
    },
    {
      title: "JPM",
      ttl: "$1000",
      crt: "$5000",
      dot: "#ff9b54",
      id: 11,
    },
    {
      title: "NVDA",
      ttl: "$1000",
      crt: "$5000",
      dot: "#43aa8b",
      id: 12,
    },
  ];
  return (
    <div className="flex-wrap items-center gap-8 py-4 md:flex">
      {arr.map((x) => (
        <div
          className="flex p-4 "
          style={{
            border: "solid 1px #dfe2e9",
            borderRadius: "6px",
            margin: "10px 0px",
          }}
          key={x.id}
        >
          <div
            style={{
              backgroundColor: x.dot,
              width: "10px",
              height: "10px",
              borderRadius: "50%",
              marginRight: "10px",
              marginTop: "5px",
            }}
          ></div>
          <div>
            <p className="text-base font-bold text-btn-color">{x.title}</p>
            <p className="text-xs text-text-light">
              Total Investment : {x.ttl}
            </p>
            <p className="text-xs text-text-light">Current Value : {x.crt}</p>
          </div>
        </div>
      ))}
    </div>
  );
}
