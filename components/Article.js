import React from "react";
import Image from "next/image";
import user1 from "../public/bitcoin_1.jpg";
import user2 from "../public/bitcoin_2.jpg";
import user3 from "../public/bitcoin_3.jpg";

import { FaGreaterThan } from "react-icons/fa";

export default function Professionals() {
	const users = [
		{
			img: user1,
			heading: "What is your opinion on Bitcoin price?",
			sub: "Amish Shiravadakar",
			lor: "Lorem Ipsum is simply dummy text of the printing and typesetting industry… ",
			by: "By Allen Lewis",
			date: "12/08/2021",
			id: 1,
		},
		{
			img: user2,
			heading: "What is your opinion on Bitcoin price?",
			lor: "Lorem Ipsum is simply dummy text of the printing and typesetting industry… ",
			sub: "Amish Shiravadakar",
			by: "By Allen Lewis",
			date: "12/08/2021",
			id: 2,
		},
		{
			img: user3,
			heading: "What is your opinion on Bitcoin price?",
			lor: "Lorem Ipsum is simply dummy text of the printing and typesetting industry… ",
			sub: "Amish Shiravadakar",
			by: "By Allen Lewis",
			date: "12/08/2021",
			id: 3,
		},
	];

	return (
		<div className="px-3 bg-light-blue">
			<div className="w-11/12 mx-auto">
				<div>
					<p className="py-8 text-2xl font-bold md:pb-24 md:pt-28 md:text-5xl">
						You might love our{" "}
						<span className="text-light-green"> Recent Articles </span>
					</p>
				</div>
				<div className="flex flex-wrap justify-center gap-8 pb-8 md:pb-16 md:gap-14">
					{users.map((x) => (
						<div key={x.id} className="mx-auto w-80">
							<Image
								className="rounded-xl"
								src={x.img}
								height={190}
								width={306}
								alt="stuff"
							></Image>
							<p className="my-5 text-xl font-bold text-navy-blue">
								{x.heading}
							</p>
							{/* <p className="text-2xl font-semibold text-navy-blue">{x.sub}</p> */}
							<p className="my-2 text-sm font-normal text-navy-blue">{x.lor}</p>
							<p className="my-2 mt-2.5 text-sm font-semibold text-navy-blue">
								{x.by}
							</p>
							<p className="flex items-center gap-2 text-xs font-semibold text-navy-blue">
								{x.date}
							</p>
						</div>
					))}
				</div>
				<div className="flex justify-center py-8 md:justify-end">
					<p className="flex items-center gap-2 text-base font-bold cursor-pointer md:text-xl text-navy-blue">
						View all <FaGreaterThan />
					</p>
				</div>
			</div>
		</div>
	);
}
