import React from "react";
import Image from "next/image";
import user1 from "../public/user_1.png";
import user2 from "../public/user_2.png";
import user3 from "../public/user_3.png";
import user4 from "../public/user_4.png";

import { FaMapMarkerAlt } from "react-icons/fa";
import { FaGreaterThan } from "react-icons/fa";

export default function Professionals() {
	const users = [
		{
			img: user1,
			prof: "Financial Professional",
			name: "Amish Shiravadakar",
			place: "Los Angeles, USA",
			id: 1,
		},
		{
			img: user2,
			prof: "Financial Professional",
			name: "Ester Bednářová",
			place: "Kolkata",
			id: 2,
		},
		{
			img: user3,
			prof: "Financial Professional",
			name: "Hirini Hakopa",
			place: "Madison",
			id: 3,
		},
		{
			img: user4,
			prof: "Financial Professional",
			name: "Tan Wuhan",
			place: "Toronto",
			id: 4,
		},
	];

	return (
		<div className="px-3 bg-light-blue">
			<div className="w-11/12 mx-auto">
				<div>
					<p className="py-8 pb-8 text-2xl font-bold md:pb-20 md:pt-32 md:text-5xl">
						Connect with top{" "}
						<span className="text-light-green">Financial Professionals</span> to
						answer your questions
					</p>
				</div>
				<div className="flex flex-wrap justify-center gap-8 pb-8 lg:flex-nowrap md:gap-16 md:pb-16 md:justify-start">
					{users.map((x) => (
						<div key={x.id}>
							<Image alt="picture" src={x.img}></Image>
							<p className="text-xs font-semibold text-navy-blue">{x.prof}</p>
							<p className="text-2xl mt-2.5 mb-3 font-semibold text-navy-blue">
								{x.name}
							</p>
							<p className="flex items-center gap-2 text-base font-normal text-navy-blue">
								<FaMapMarkerAlt /> {x.place}
							</p>
						</div>
					))}
				</div>
				<div className="flex justify-center py-8 md:justify-end">
					<p className="flex items-center gap-2 text-base font-bold cursor-pointer md:text-xl text-navy-blue">
						View all <FaGreaterThan />
					</p>
				</div>
			</div>
		</div>
	);
}
