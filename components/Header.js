import React from "react";
import Image from "next/image";
export default function Header() {
	return (
		<div className="flex flex-col items-center justify-center text-center h-[85vh]">
			<div className="w-11/12 px-4 mx-auto md:w-9/12">
				<p className="text-4xl font-bold md:text-6xl text-navy-blue">
					Wimple is <span className="text-light-green">wealth</span> made{" "}
					<span className="text-light-green"> simple</span>{" "}
				</p>
				<p className="mt-10 mb-10 text-xl font-normal text-light-brown">
					Find Top Rated Financial Professionals
				</p>
				<form>
					<div className="relative">
						<div className="absolute inset-y-0 left-0 flex items-center pl-6 pointer-events-none -top-8 md:top-0">
							<Image
								alt="stuff"
								src={"/icon-search.svg"}
								height={20}
								width={20}
							></Image>
						</div>
						<input
							type="search"
							id="default-search"
							className="block w-full p-3 text-sm text-gray-900 border rounded-t-lg rounded-b-none md:text-lg md:p-8 border-btn-border md:rounded-lg bg-btn-light outline-0"
							style={{ paddingLeft: "60px" }}
							placeholder="Ex- 401K, Financial Professional, Tax Advice"
							required
						/>
						<button
							type="submit"
							className="w-full px-4 py-2 text-xs font-semibold text-white rounded-b-lg bg-btn-color md:hidden"
						>
							Search
						</button>
						<button
							type="submit"
							className="hidden md:block text-white w-44 absolute right-2.5 bottom-4 bg-btn-color  font-semibold rounded-lg text-base p-5"
						>
							Search
						</button>
					</div>
				</form>
			</div>
		</div>
	);
}
