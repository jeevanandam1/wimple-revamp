import React from "react";
import Image from "next/image";
import Link from "next/link";

export default function Footer() {
	const navLinks = [
		{
			link: "Professionals",
			nav: "#",
		},
		{
			link: "Forums",
			nav: "#",
		},
		{
			link: "Articles",
			nav: "#",
		},
		{
			link: "How it works",
			nav: "#",
		},
		{
			link: "Calculators",
			nav: "calculator",
		},
		{
			link: "Contact us",
			nav: "#",
		},
	];

	return (
		<div>
			<footer>
				<div className="items-center justify-between px-4 py-6 mx-auto md:w-11/12 md:flex">
					<div className="py-2 text-center md:py-0">
						<Image alt="picture" src={"/logo.png"} height={50} width={140} />
					</div>
					<ul className="flex items-center flex-col gap-0.5 md:flex-row md:items-start">
						{navLinks.map((x, index) => (
							<li className="md:px-2.5" key={index}>
								<Link href={`/${x.nav}`}>
									<a className="font-semibold text-navy-blue">{x.link}</a>
								</Link>
							</li>
						))}
					</ul>
				</div>
				<div className="py-2 text-center border-t md:py-4">
					<p className="py-6 font-semibold text-light-brown">
						2021 All rights reserved
					</p>
				</div>
			</footer>
		</div>
	);
}
