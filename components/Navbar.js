import React from "react";
import { FaAngleDown } from "react-icons/fa";
import { FiMenu } from "react-icons/fi";
import { AiOutlineClose } from "react-icons/ai";
import Image from "next/image";
import Link from "next/link";

import { useState } from "react";

export default function Navbar() {
	const navLinks = [
		{
			nav: "Professionals",
			href: "/",
		},
		{
			nav: "Forums",
			href: "/",
		},
		{
			nav: "Articles",
			href: "/",
		},
		{
			nav: "Events",
			href: "/",
		},
		{
			nav: "Pricing",
			href: "/",
		},
	];

	const [menu, setMenu] = useState(false);

	const handleClick = () => {
		setMenu(!menu);
	};
	return (
		<div>
			<header className="border-b border-slate-100">
				<div className="flex items-center justify-between h-24 px-4 mx-auto xl:w-11/12">
					<Image src={"/logo.png"} alt="wimple-logo" height={50} width={146} />
					<ul className="items-center hidden gap-0.5 md:flex">
						{navLinks.map((x, index) => (
							<li className="px-2.5" key={index}>
								<Link href="#">
									<a className="font-semibold text-navy-blue font-base">
										{x.nav}
									</a>
								</Link>
							</li>
						))}
						<li className="flex items-center gap-3 px-3 py-2">
							<Link href="/dashboard">
								<a className="font-semibold text-btn-secondary">Login</a>
							</Link>
							<FaAngleDown className="font-semibold text-btn-secondary" />
						</li>
						<li className="flex items-center gap-3 px-3 py-2 rounded bg-btn-secondary text-navy-blue">
							<Link href="#">
								<a className="font-semibold">Sign Up</a>
							</Link>
							<FaAngleDown />
						</li>
					</ul>
					{!menu && (
						<FiMenu className="block md:hidden" onClick={handleClick} />
					)}
					{menu && <AiOutlineClose onClick={handleClick} />}
				</div>
				{menu && (
					<ul className="absolute z-40 flex flex-col w-full gap-2 px-2 py-3 bg-white border border-solid">
						{navLinks.map((x, index) => (
							<li className="px-2.5" key={index}>
								<Link href="#">
									<a className="font-semibold text-navy-blue font-base">
										{x.nav}
									</a>
								</Link>
							</li>
						))}
						<li className="px-2.5">
							<Link href="/dashboard">
								<a className="font-semibold text-navy-blue font-base">Login</a>
							</Link>
						</li>
						<li className="flex items-center justify-between gap-3 px-3 py-2 rounded bg-btn-secondary text-navy-blue">
							<Link href="#">
								<a className="font-bold">Sign Up</a>
							</Link>
							<FaAngleDown />
						</li>
					</ul>
				)}
			</header>
		</div>
	);
}
