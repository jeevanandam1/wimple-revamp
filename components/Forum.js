import React from "react";

import { FaGreaterThan } from "react-icons/fa";
import { AiOutlineArrowRight } from "react-icons/ai";

export default function Forum() {
	const users = [
		{
			ins: "Insurance",
			txt: "Is Whole life insurance is a good investment?",
			ans: "356 answers",
			id: 1,
		},
		{
			ins: "Insurance",
			txt: "Is Whole life insurance is a good investment?",
			ans: "356 answers",
			id: 2,
		},
		{
			ins: "Insurance",
			txt: "Is Whole life insurance is a good investment?",
			ans: "356 answers",
			id: 3,
		},
	];

	return (
		<div className="px-3 ">
			<div className="w-11/12 mx-auto">
				<div>
					<p className="py-8 text-2xl font-bold md:pb-24 md:pt-28 md:text-5xl">
						Most Active <span className="text-light-green">Forum Topics</span>
					</p>
				</div>
				<div className="flex flex-wrap gap-8 pb-8 lg:flex-nowrap md:pb-16 md:gap-14 ">
					{users.map((x) => (
						<div
							className="h-auto p-8 mx-auto border-4 w-96 border-border-color rounded-2xl"
							key={x.id}
						>
							<p className="text-base font-semibold text-light-green">
								{x.ins}
							</p>
							<p className="py-4 text-2xl font-bold text-navy-blue">{x.txt}</p>
							<button className="flex items-center justify-between w-full p-4 font-semibold text-white rounded bg-navy-blue">
								{x.ans}
								<AiOutlineArrowRight />
							</button>
						</div>
					))}
				</div>
				<div className="flex justify-center py-8 md:justify-end">
					<p className="flex items-center gap-2 text-base font-bold cursor-pointer md:text-xl text-navy-blue">
						View all <FaGreaterThan />
					</p>
				</div>
			</div>
		</div>
	);
}
