import React from "react";

export default function Form() {
	const btnArr = [
		{
			text: "Savings Calculator",
			id: 1,
		},
		{
			text: "401K Calculator",
			id: 2,
		},
		{
			text: "Savings Calculator",
			id: 3,
		},
		{
			text: "401K Calculator",
			id: 4,
		},
	];

	return (
		<section className="bg-light-green">
			<div className="flex flex-wrap items-center w-11/12 min-h-screen gap-8 px-2 py-16 mx-auto lg:gap-4 md:flex-nowrap xl:gap-20">
				<div className="p-4 md:basis-3/5">
					<p className="text-2xl font-bold md:text-5xl text-navy-blue">
						A <span className="text-white"> &#8220;Calculated&#8221; </span>{" "}
						Approach to your Finances
					</p>
					<p className="py-8 text-base font-normal text-navy-blue">
						Wimple financial calculators are a great way to understand your
						financial picture and prepare you for a more personalized meeting
						with a financial professional. These tools will help you understand
						how to reach your goals.
					</p>
					<p className="py-6 text-xl font-semibold text-navy-blue">
						Try our calculators 👇
					</p>
					<div className="grid gap-6 sm:grid-cols-2">
						{btnArr.map((x) => (
							<div key={x.id}>
								<button className="w-full p-4 text-base font-semibold text-white rounded bg-btn-color">
									{x.text}
								</button>
							</div>
						))}
					</div>
				</div>
				<div className="md:basis-2/5">
					<div className="p-8 mb-2 text-center bg-white md:m-4 rounded-xl">
						<p className="py-4 my-4 text-2xl font-bold text-navy-blue">
							401K Calculator
						</p>
						<button className="w-full px-8 py-4 mx-auto text-2xl font-bold md:py-8 md:px-16 md:text-4xl md:w-96 text-brown-color rounded-xl bg-light-blue">
							$8,203
						</button>
						<p className="py-2 my-4 text-base font-bold text-left text-btn-color">
							MONTHLY SAVINGS CONTRIBUTION REQUIRED
						</p>
						<p className="py-2 text-sm font-normal text-left text-text-gray">
							Save this amount per month and you will hit your target if you are
							able to maintain this expected rate of return throughout the next
							5 years.{" "}
						</p>
						<button className="p-4 mt-16 text-base font-bold text-white rounded md:text-xl bg-btn-color">
							Find a Financial Professional
						</button>
					</div>
				</div>
			</div>
		</section>
	);
}
