module.exports = {
	mode: "jit",
	content: [
		"./pages/**/*.{js,ts,jsx,tsx}",
		"./components/**/*.{js,ts,jsx,tsx}",
	],
	theme: {
		extend: {
			colors: {
				"navy-blue": "#04004d",
				"light-green": "#00d385",
				"light-brown": "#303030",
				"form-border": "#cfd8e3",
				"form-background": "#ebeaeb",
				"btn-color": "#04004d",
				"btn-secondary": "#00d385",
				"light-blue": "#edf2f7",
				"border-color": "#f9f9f9",
				"brown-color": "#303030",
				"text-gray": "#6c7e8e",
				"white-gray": "#f3f4f6",
				"navy-dark": "#111827",
				"text-light": "#615f81",
				"btn-light": "#f9f9f9",
				"btn-border": "#cfd8e3",
				"light-secondary": "#6b7280",
			},
			screens: {
				sm: "480px",
				md: "769px",
			},
		},
	},
	plugins: [],
};
